
/**
 * Action to add candidate
 * @param candidate 
 */
export const addCandidate = (candidate: object) => {
    return {
        payload: candidate,
        type: 'ADD_CANDIDATE'
    }
}

/**
 * update candidate action
 * @param candidate 
 * @param rowIndex 
 */
export const updateCandidate = (candidate: object, rowIndex: number) => {
    return {
        payload: candidate,
        rowIndex: rowIndex,
        type: 'UPDATE_CANDIDATE'
    }
}

/**
 * delete candidate action
 * @param candidate 
 * @param rowIndex 
 */
export const deleteCandidate = (candidate: object, rowIndex: number) => {
    return {
        payload: candidate,
        rowIndex: rowIndex,
        type: 'DELETE_CANDIDATE'
    }
}

/**
 * action to filter candidates by key and value
 * @param candidate 
 * @param key 
 * @param value 
 */
export const filterCandidates = (candidate: object, key: string, value: string) => {
    return {
        payload: candidate,
        key: key,
        value: value,
        type: 'FILTER_CANDIDATE'
    }
}

/**
 * Pagination
 * @param candidate 
 * @param pageInterval 
 */
export const paginate = (candidate: object, pageInterval: number) => {
    return {
        payload: candidate,
        pageInterval: pageInterval,
        type: 'PAGINATE'
    }
}
