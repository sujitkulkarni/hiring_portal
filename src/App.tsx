
import * as React from "react";
import CandidateTable from "./components/candidate-table/CandidateTable";
import AddCandidate from "./components/add-candidate/AddCandidate";
import CandidateControls from "./components/candidate-controls/CandidateControls";
import Header from "./components/header/Header";
import { Position, Toaster } from "@blueprintjs/core";
import "./App.css";

/**
 * App component states' interface
 */
interface IAppStates {
  isLoading: boolean;
  inputs: object;
  deleteItems: object;
  pageInterval: number;
}

/**
 * Wrapper Component
 */
class App extends React.Component<any, IAppStates> {
  private overlay = React.createRef<any>();
  
  constructor(props: any) {
    super(props);
    this.state = {
      isLoading: false,
      inputs: {},
      deleteItems: {},
      pageInterval: 0
    };
  }

  // open overlay to add new candidate
  onAddCandidate = () => {
    this.overlay.current.handleOpen();
  };

  // data added from overlay
  onAddData = (inputs: object) => {
    console.log("app", inputs);
    this.setState({ inputs: [inputs] });
  };

  // delete data of a selected row
  onDeleteCandidate = (selectedRow: object[]) => {
    this.setState({ deleteItems: selectedRow });
  };

  // set data state and,
  // show toast when data fetched
  onFetchData = (data: object[]) => {
    this.setState({ inputs: data});
    AppToaster.clear();
  };

  // pagination interval changed
  onPageIntervalChange = (interval: number) => {
    this.setState({pageInterval: interval});
  }

  // fetch data when component mounts  
  public componentDidMount = () => {
    AppToaster.show({
      message: 'Fetching data...'
    });
    console.log("componentDidMount");
    fetch("https://api.myjson.com/bins/1ahlps")
      .then(response => response.json())
      .then(data => {
        console.log("fetched", data);
        this.onFetchData(data);
      });
  };

  //render component
  public render() {
    return (
      <div className="App container-fluid">
        <Header/>
        <section className="mt-4">
          <div className="row">
            <CandidateTable
              inputs={this.state.inputs}
              deleteItems={this.state.deleteItems}
              pageInterval={this.state.pageInterval}
            />
          </div>          
        </section>
        <footer className="row ">
          <div className="col-12 mt-4 mb-2">
            <CandidateControls
              onAddCandidate={this.onAddCandidate.bind(this)}
              onDeleteCandidate={this.onDeleteCandidate.bind(this)}
              onPageIntervalChange={this.onPageIntervalChange.bind(this)}
            />
          </div>
          <div className="col-12 mt-4">
            <small>*Double click on any cell to edit</small><br/>
            <small>*Select a row to delete</small>
          </div>
        </footer>
        <AddCandidate ref={this.overlay} addData={this.onAddData} />
      </div>
    );
  }
}

export default App;

/**
 * Toaster
 */
export const AppToaster = Toaster.create({
  className: "toaster",
  position: Position.TOP,
});
