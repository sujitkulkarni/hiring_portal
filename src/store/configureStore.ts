/**
 * Store creator
 */
import { createStore } from 'redux';
import rootReducer from '../reducers';

export const configureStore = (initialState: any) => {
    return createStore(rootReducer, initialState, (<any>window).__REDUX_DEVTOOLS_EXTENSION__ && (<any>window).__REDUX_DEVTOOLS_EXTENSION__());
}