import { Button, Classes, Navbar, NavbarDivider, NavbarGroup, NavbarHeading } from "@blueprintjs/core";
import * as React from "react";

/* 
  Header component with navbar
*/
class Header extends React.Component {
  public render() {
    return (
      <Navbar className="mt-2">
        <NavbarGroup align="center">
          <NavbarHeading>iHire</NavbarHeading>
          <NavbarDivider />
          <Button className={Classes.MINIMAL} icon="home" text="Home" />          
        </NavbarGroup>
      </Navbar>
    );
  }
}

export default Header;
