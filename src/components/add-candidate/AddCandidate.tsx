import * as React from "react";
import { Classes, Overlay, Card, Elevation, Button, FormGroup, InputGroup, FileInput, HTMLSelect  } from "@blueprintjs/core";
import { Candidate } from '../../models/candidateModel';

/**
 * Interface for component states
 */
interface IAddCandidateState {
    isOpen: boolean,
    candidateName: string,
    location: string,
    technology: string,
    experience: string,
    position: string,
    status: string,
    interviewer: string,
    resume: any
}

/**
 * Dropdowns' options
 */
const technologies: string[] = ['Angular', 'React', 'Java', 'DevOps'];
const designations: string[] = ['SE', 'SSE', 'SA', 'SSA', 'PM'];
const rounds: string[] = ['1st', '2nd', 'Final'];

/**
 * Add Candidate Component
 */
class AddCandidate extends React.Component<any, IAddCandidateState> {
  constructor(props: any) {
    super(props);
    this.state={
        isOpen: false,
        candidateName: '',
        location: '',
        technology: '',
        experience: '',
        position: '',
        status: '',
        interviewer: '',
        resume: ''
    };
    this.collectInputs = this.collectInputs.bind(this);
  }

	// overlay opened
  handleOpen = () => { this.setState({ isOpen: true})}

	// overlay closed
  private handleClose = () => this.setState({ isOpen: false });

	// get user inputs
  private collectInputs = (e: any): any => {
      if(e){
          this.setState({[e.target.name]: e.target.value} as any);
      }
  }

	// render dropdown options
  private getOptions = (array: any[]) => {
      return array.map((val: string, index: number) => <option value={val} key={index}>{val}</option>)
  }

	// pass collected data to parent
  private submitData = () => {
      const candidate = new Candidate(this.state);
      this.props.addData(candidate);
  }

	// render component
  public render() {
    return (
        <div className="overlay_wrapper">        
            <Overlay isOpen={this.state.isOpen} onClose={this.handleClose} canOutsideClickClose={true} className={Classes.OVERLAY_SCROLL_CONTAINER }>
                <Card interactive={true} elevation={Elevation.TWO} className="col-md-8">
                    <h5><a href="#">Add Candidate</a></h5>
                    <FormGroup
                        label="Name"
                        labelFor="candidate-name"
                        labelInfo="(required)"
                    >
                        <InputGroup id="candidate-name" name="candidateName" placeholder="Placeholder text" onChange={this.collectInputs}/>
                    </FormGroup>
                    <FormGroup
                        label="Location"
                        labelFor="candidate-location"
                    >
                        <InputGroup id="candidate-location" name="location" placeholder="Placeholder text" onChange={this.collectInputs}/>
                    </FormGroup>
                    <FormGroup
                        label="Technology"
                        labelFor="candidate-technology"
                    >
                        <HTMLSelect fill={true} name="technology" onChange={this.collectInputs}>
                            <option defaultValue="Select">Select</option>
                            {this.getOptions(technologies)}
                        </HTMLSelect>
                    </FormGroup>
                    <FormGroup
                        label="Experience"
                        labelFor="candidate-experience"
                    >
                        <InputGroup id="candidate-experience" name="experience" placeholder="Placeholder text" onChange={this.collectInputs}/>
                    </FormGroup>
                    <FormGroup
                        label="Designation"
                        labelFor="candidate-designation"
                    >
                        <HTMLSelect fill={true} name="position" onChange={this.collectInputs}>
                            <option defaultValue="Select">Select</option>
                            {this.getOptions(designations)}
                        </HTMLSelect>
                    </FormGroup>
                    <FormGroup
                        label="Status"
                        labelFor="candidate-status"
                    >
                        <HTMLSelect fill={true} name="status" onChange={this.collectInputs}>
                            <option defaultValue="Select">Select</option>
                            {this.getOptions(rounds)}
                        </HTMLSelect>
                    </FormGroup>
                    <FormGroup
                        label="Interviewer"
                        labelFor="candidate-interviewer"
                    >
                        <InputGroup id="candidate-status" name="interviewer" placeholder="Placeholder text" onChange={this.collectInputs}/>
                    </FormGroup>
                    <FormGroup
                        label="Resume"
                        labelFor="candidate-resume"
                    >
                        <FileInput disabled={false} text="Choose file..." className="bp3-fill" onChange={this.collectInputs}/>
                    </FormGroup>
                    <Button onClick={this.submitData}>Submit</Button>
                    <Button onClick={this.handleClose}>Close</Button>
                </Card>
            </Overlay>
        </div>
    );
  }
}

export default AddCandidate;
