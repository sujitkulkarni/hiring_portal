import * as React from "react";
import { HTMLSelect, Button, ButtonGroup } from "@blueprintjs/core";

/**
 * Interface for component props 
 */
interface ICandidateControlsProps {
  onAddCandidate: any;
  onDeleteCandidate: any;
  onPageIntervalChange: any;
}

// Number of entries to show at a time
const showAtATime = [5, 10];

/**
 * Candidate Controls component
 */
class CandidateControls extends React.Component<ICandidateControlsProps, any> {
  constructor(props: any) {
    super(props);
  }

  // tell parent to add candidate
  addCandidate() {
    this.props.onAddCandidate(true);
  }

  // tell parent to delete candidate
  deleteCandidate() {
    this.props.onDeleteCandidate();
  }

  // render page interval options
  private getPageIntervals = () => {
    return showAtATime.map((interval: number, index: number) => (
      <option key={index} value={interval}>
        {interval}
      </option>
    ));
  };

  // tell parent to change page interval
  private setPageInterval = (e: any) => {
    this.props.onPageIntervalChange(e.target.value);
  };

  // render component
  public render() {
    return (
      <div className="row">
        <div className="col-4 offset-4 mt-2">
          <ButtonGroup minimal={false} fill={true}>
            <Button icon="plus" onClick={this.addCandidate.bind(this)}>
              Add
            </Button>
            <Button icon="trash" onClick={this.deleteCandidate.bind(this)}>
              Delete
            </Button>
            <HTMLSelect onChange={this.setPageInterval}>
              <option defaultValue="Show" value={-1}>Show All</option>
              {this.getPageIntervals()}
            </HTMLSelect>
          </ButtonGroup>
        </div>
      </div>
    );
  }
}

export default CandidateControls;
