import * as React from "react";
import { ControlGroup, InputGroup, HTMLSelect } from "@blueprintjs/core";

/**
 * Table Header & Controls
 */
class CandidateTableHeader extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  // render filter options
  private getFilterByOptions = () => {
    return this.props.dataKeys.map(
      (item: { key: string; value: string }, index: number) => {
        return (
          <option key={index} value={item.value}>
            {item.value}
          </option>
        );
      }
    );
  };

  //  render component
  public render = () => {
    return (
      <div className="row">
        <div className="col-9">
          <h1 className="bp3-heading">Candidates</h1>
        </div>
        <div className="col-3">
          <ControlGroup fill={true} vertical={false}>
            <HTMLSelect onChange={this.props.onFilterChange}>
              <option defaultValue="Select">Filter By</option>
              {this.getFilterByOptions()}
            </HTMLSelect>
            <InputGroup
              placeholder="What are you looking for?"
              onChange={this.props.onFilterValueChange}
              disabled={!this.props.isDisabled}
            />
          </ControlGroup>
        </div>
      </div>
    );
  };
}

export default CandidateTableHeader;
