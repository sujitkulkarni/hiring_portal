import {
  Column,
  ColumnHeaderCell,
  EditableCell,
  Table,
  RegionCardinality
} from "@blueprintjs/table";
import * as React from "react";
import { connect } from "react-redux";
import * as candidateActions from "../../actions/actions";
import CandidateTableHeader from "./CandidateTableHeader";

/* 
  Interface defining props
*/
interface IProps {
  candidateData: any;
  filteredOutData: any;
  addCandidateData: any;
  updateCandidateData: any;
  deleteCandidateData: any;
  filterCandidateData: any;
  paginate: any;
  inputs: any;
  deleteItems: any;
  pageInterval: number;
}

/* 
  Component to display candidate data in a table
*/
class CandidateTable extends React.Component<IProps, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      selectedRow: [],
      filterKey: '',
      filterValue: ''
    };
  }

  // expect new props
  public componentWillReceiveProps(nextProps: any) {
    if (nextProps.inputs !== this.props.inputs) {
      this.props.addCandidateData(nextProps.inputs);
    }

    if (nextProps.deleteItems !== this.props.deleteItems) {
      this.props.deleteCandidateData(
        this.props.candidateData,
        this.state.selectedRow
      );
    }

    if(nextProps.pageInterval !== this.props.pageInterval){
      let pageInterval = nextProps.pageInterval;
      if(nextProps.pageInterval <= 0){
        pageInterval = this.props.candidateData.length + this.props.filteredOutData.length;
      }
      let candidateData = this.props.candidateData.concat(this.props.filteredOutData);
      this.props.paginate(candidateData, pageInterval);
    }
  }

  
  //  Table headers  
  public candidateDataKeys = [
    { key: "candidateName", value: "Name" },
    { key: "location", value: "Location" },
    { key: "technology", value: "Technology" },
    { key: "experience", value: "Experience" },
    { key: "position", value: "Position" },
    { key: "status", value: "Status" },
    { key: "interviewer", value: "Interviewer" },
    { key: "resume", value: "Resume" }
  ];

  // set cell text and update state
  protected setCellData = (rowIndex: number, colIndex: number) => {
    return (value: string) => {
      let data = this.props.candidateData.filter(
        (obj: object, index: number) => {
          return index === rowIndex;
        }
      )[0];
      console.log(data);
      data[this.candidateDataKeys[colIndex].key] = value;
      this.props.updateCandidateData(data, rowIndex);
    };
  };

  // Render Cells
  protected cellRenderer = (rowIndex: number, colIndex: number) => {
    if (rowIndex < this.props.candidateData.length) {
      return (
        <EditableCell
          value={
            this.props.candidateData[rowIndex][
              this.candidateDataKeys[colIndex].key
            ]
          }
          onConfirm={this.setCellData(rowIndex, colIndex)}
        />
      );
    } else {
      return <EditableCell onConfirm={this.setCellData(rowIndex, colIndex)} />;
    }
  };

  
  //   Get Column Headers  
  protected columnHeaderRenderer = (columnIndex: number) => {
    return (
      <ColumnHeaderCell name={this.candidateDataKeys[columnIndex].value} />
    );
  };

  // handle row seletion
  private onSelect = (selectedRegions: any) => {
    console.log("onSelect", selectedRegions);
    this.setState({ selectedRow: selectedRegions });
  };

  // handle filter dropdown change
  private onFilterChange = (e: any) => {
    const keyVal = this.candidateDataKeys.find((data: {key: string, value: string}) => data.value === e.target.value);
    console.log(keyVal);
    this.setState({filterKey: keyVal ? keyVal.key: ''});    
  }

  // handle filter text input
  private onFilterValueChange = (e: any) => {
    if(this.state.filterKey){
      this.props.filterCandidateData(this.props.inputs, this.state.filterKey, e.target.value)
    } 
  }

  // render table
  private getTable = () => {
    const numRows = this.props.candidateData.length;
    if(numRows){
      return (
        <Table
            numRows={numRows}
            selectionModes={[
              RegionCardinality.CELLS,
              RegionCardinality.FULL_ROWS
            ]}
            onSelection={this.onSelect}
          >
            <Column
              cellRenderer={this.cellRenderer}
              columnHeaderCellRenderer={this.columnHeaderRenderer}
            />
            <Column
              cellRenderer={this.cellRenderer}
              columnHeaderCellRenderer={this.columnHeaderRenderer}
            />
            <Column
              cellRenderer={this.cellRenderer}
              columnHeaderCellRenderer={this.columnHeaderRenderer}
            />
            <Column
              cellRenderer={this.cellRenderer}
              columnHeaderCellRenderer={this.columnHeaderRenderer}
            />
            <Column
              cellRenderer={this.cellRenderer}
              columnHeaderCellRenderer={this.columnHeaderRenderer}
            />
            <Column
              cellRenderer={this.cellRenderer}
              columnHeaderCellRenderer={this.columnHeaderRenderer}
            />
            <Column
              cellRenderer={this.cellRenderer}
              columnHeaderCellRenderer={this.columnHeaderRenderer}
            />
            <Column
              cellRenderer={this.cellRenderer}
              columnHeaderCellRenderer={this.columnHeaderRenderer}
            />
          </Table>
      )
    } else {
      return (
        <div className="text-danger">Waiting...</div>        
      )
    }
  }

  /* 
    Render
  */
  public render = () => {
    console.log("render", this.state);
    
    return (
      <div className="col-12">
        <CandidateTableHeader 
          dataKeys={this.candidateDataKeys} 
          onFilterChange={this.onFilterChange.bind(this)} 
          onFilterValueChange={this.onFilterValueChange.bind(this)}
          isDisabled={this.state.filterKey ? true: false}
        />
        {this.getTable()}
      </div>
    );
  }
}

/* 
  Mapper function for props
*/
const mapStateToProps = (state: any) => {
  console.log("mapStateToProps", state);
  return {
    candidateData: state.candidateData.candidates,
    filteredOutData: state.candidateData.filteredOutData
  };
};

/* 
  Mapper function for dispatch event
*/
const mapDispatchToProps = (dispatch: any) => {
  return {
    addCandidateData: (data: object) =>
      dispatch(candidateActions.addCandidate(data)),
    updateCandidateData: (data: object, rowIndex: number) =>
      dispatch(candidateActions.updateCandidate(data, rowIndex)),
    deleteCandidateData: (data: object, region: any) =>
      dispatch(candidateActions.deleteCandidate(data, region)),
    filterCandidateData: (data: object, key: string, value: string) => 
      dispatch(candidateActions.filterCandidates(data, key, value)),
    paginate: (data: object, pageInterval: number) =>
      dispatch(candidateActions.paginate(data, pageInterval))
  };
};

/* 
  Connect component
*/
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CandidateTable);
