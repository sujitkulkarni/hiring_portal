export interface ICandidate{
    candidateName: string;
    location: string;
    technology: string;
    experience: string;
    position: string;
    status: string;
    interviewer: string;
    resume: any;
}

export class Candidate implements ICandidate{
    candidateName: string;
    location: string;
    technology: string;
    experience: string;
    position: string;
    status: string;
    interviewer: string;
    resume: any;

    constructor(obj: ICandidate){
        this.candidateName = obj.candidateName;
        this.location = obj.location;
        this.technology = obj.technology;
        this.experience = obj.experience;
        this.position = obj.position;
        this.status = obj.status;
        this.interviewer = obj.interviewer;
    }
}