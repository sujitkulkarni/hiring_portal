/**
 * Reducer combiner
 */
import { combineReducers } from 'redux';
import candidateData from '../reducers/candidate_reducer';

export default combineReducers({candidateData});