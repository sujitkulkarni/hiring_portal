/**
 * Interface for action object
 */
interface IAction {
  type?: string;
  payload?: any;
  rowIndex: number;
  key: string;
  value: string;
  pageInterval: number;
}

/**
 * Set initial state
 */
const initialState = {
  candidates: [],
  filteredOutData: []
};

/**
 * Reducer
 */
export default (state = initialState, action: IAction) => {
  switch (action.type) {
    case "ADD_CANDIDATE":
      return {
        ...state,
        candidates: state.candidates.concat(...action.payload)
      };

    case "UPDATE_CANDIDATE":
      const newState = Object.assign({}, state);
      const newValues = newState.candidates.map(
        (data: object, index: number) => {
          if (index === action.rowIndex) {
            data = action.payload;
          }
          return data;
        }
      );
      console.log("UPDATE_CANDIDATE", newValues);
      return Object.assign({}, state, { candidates: newValues });

    case "DELETE_CANDIDATE":
      const numberOfRows = action.rowIndex[0].rows;
      return Object.assign({}, state, {
        candidates: state.candidates.filter(
          (data: object, index: number): any => {
            if (
              index < Math.min(...numberOfRows) ||
              index > Math.max(...numberOfRows)
            ) {
              return data;
            }
          }
        )
      });

    case "FILTER_CANDIDATE":
      return Object.assign({}, state, {
        candidates: action.payload.filter(
          (data: object): any => {
            const regex = new RegExp(action.value, "i");
            console.log("search", data, action.key);
            if (data[action.key].search(regex) > -1) {
              return data;
            }
          }
        )
      });

    case "PAGINATE":      
      const filteredData = Object.assign({}, state, {
        candidates: action.payload.filter(
          (data: object, index: number): any => {
            if(data){
              if (index < action.pageInterval) {
                return data;
              } else {
                console.log('FILTER PAGINATE', data, action.pageInterval);              
              }
            }
          }
        )
      });
      return Object.assign({}, filteredData, {filteredOutData: action.payload.filter((data: object, index: number): any => (index >= action.pageInterval))});

    default:
      return state;
  }
};
